﻿using System;

using Xamarin.Forms;

namespace NewsApp
{
	public class TabButton : StackLayout
	{
		public EventHandler Clicked;

		private Label Label;
		private BoxView BoxView;
		private Color ActiveTextColor = Color.Black;
		private Color InactiveTextColor = Color.FromRgb(153, 153, 153);
		private Color ActiveLineColor = Color.Purple;
		private Color InactiveLineColor = Color.FromRgb(204, 204, 204);
		private bool active = false;

		public bool Active
		{
			get
			{
				return active;
			}
			set
			{
				this.Label.TextColor = value ? ActiveTextColor : InactiveTextColor;
				this.Label.FontAttributes = value ? FontAttributes.Bold : FontAttributes.None;
				this.BoxView.BackgroundColor = value ? ActiveLineColor : InactiveLineColor;
				active = value;
			}
		}

		public TabButton()
		{
			Padding = new Thickness(0, 10, 0, 0);
			Spacing = 10;
			BackgroundColor = Color.White;
			var tgr = new TapGestureRecognizer();
			tgr.Tapped += (sender, e) =>
			{
				if (Clicked != null)
				{
					Clicked(this, e);
				}
			};
			GestureRecognizers.Add(tgr);
			Label = new Label()
			{
				FontSize = 17.5,
				TextColor = InactiveTextColor,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			this.Children.Add(Label);
			BoxView = new BoxView()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End,
				HeightRequest = 6,
				BackgroundColor = InactiveLineColor
			};
			this.Children.Add(BoxView);
		}

		public string Text
		{
			get
			{
				return Label.Text;
			}
			set
			{
				Label.Text = value;
			}
		}
	}
}

