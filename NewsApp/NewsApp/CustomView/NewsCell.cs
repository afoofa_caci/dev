﻿using System;

using Xamarin.Forms;

namespace NewsApp
{
	//This is a custom Cell. 
	public class NewsCell : ViewCell
	{
		Label lblTitle;
		Label lblDesc;
		Label lblLink;
		Label lblPubDate;
		Image imgThumbnail;

		public NewsCell()
		{
			lblTitle = new Label
			{
				FontSize = 15
			};

			lblDesc = new Label
			{
				FontSize = 12.5,
				TextColor = Color.Gray,
				LineBreakMode = LineBreakMode.TailTruncation
			};

			lblLink = new Label
			{
				FontSize = 8,
				TextColor = Color.Blue,
			};

			lblPubDate = new Label
			{
				FontSize = 8,
				TextColor = Color.Gray,
			};
			imgThumbnail = new Image()
			{
				VerticalOptions = LayoutOptions.Start
			};

			var imageLayout = new StackLayout
			{
				Children = {imgThumbnail, lblPubDate}
			};

			var dataLayout = new StackLayout
			{
				Padding = 5,
				Children = { lblTitle, lblDesc, lblLink}
			};

			var grid = new Grid()
			{
				Padding = 5,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			grid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
			grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(3, GridUnitType.Star) });
			grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(7, GridUnitType.Star) });

			grid.Children.Add(imageLayout, 0, 0);
			grid.Children.Add(dataLayout, 1, 0);

			this.Height = 120;
			this.View = grid;
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			var vm = BindingContext as NewsDetail;

			if (vm != null)
			{
				lblTitle.Text = vm.Title;

				lblDesc.Text = vm.Description;

				lblLink.Text = vm.Link;

				lblPubDate.Text = Util.AsRelativeDate(vm.PubDate);

				if (!string.IsNullOrEmpty(vm.ImageUrl))
				{
					imgThumbnail.Source = ImageSource.FromUri(new Uri(vm.ImageUrl));
				}
			}
		}
	}
}

