﻿using System;

using Xamarin.Forms;

namespace NewsApp
{
	public class CustomTab : Grid
	{
		private TabButton btnUKNews;
		private TabButton btnTechNews;

		public event Action UKNewClicked;
		public event Action TechnolgyClicked;

		public CustomTab()
		{
			btnUKNews = new TabButton()
			{
				Text = "UK News",
				Active = true
			};

			btnTechNews = new TabButton()
			{
				Text = "Technology News",
			};

			btnUKNews.Clicked += HandleUKNewsSelected;
			btnTechNews.Clicked += HandleTechNewsSelected;

			Padding = 0;
			RowSpacing = 0;
			ColumnSpacing = 1;
			BackgroundColor = Color.Blue;
			RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
			ColumnDefinitions.Add(new ColumnDefinition());
			ColumnDefinitions.Add(new ColumnDefinition());

			BackgroundColor = Color.White;
			HorizontalOptions = LayoutOptions.FillAndExpand;
			VerticalOptions = LayoutOptions.Start;

			Children.Add(btnUKNews, 0, 1);
			Children.Add(btnTechNews, 1, 1);
		}

		private void HandleTechNewsSelected(object sender, EventArgs e)
		{
			if (btnTechNews.Active)
				return;

			if (TechnolgyClicked != null)
				TechnolgyClicked();
			btnUKNews.Active = false;
			btnTechNews.Active = true;
		}

		private void HandleUKNewsSelected(object sender, EventArgs e)
		{
			if (btnUKNews.Active)
				return;

			if (UKNewClicked != null)
				UKNewClicked();
			btnUKNews.Active = true;
			btnTechNews.Active = false;
		}
	}
}

