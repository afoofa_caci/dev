﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace NewsApp
{
	/// <summary>
	/// This Manager will take the responsiblities for whatever todo with RSS feed only. 
	/// </summary>
	public static class NewsManager
	{

		const string BBCUK = "http://feeds.bbci.co.uk/news/uk/rss.xml";
		const string BBCTechnolgy = "http://feeds.bbci.co.uk/news/technology/rss.xml";


		public static async Task<List<NewsDetail>> Parser(NewsSource newsSource)
		{
			var targetUrl = newsSource == NewsSource.BBCUK ? BBCUK : BBCTechnolgy;

			try
			{
				using (var httpClient = new HttpClient())
				{
					var rssUrl = new Uri(targetUrl);
					var xml = await httpClient.GetStringAsync(rssUrl);

					if (xml != null)
					{
						XDocument doc = XDocument.Parse(xml);

						var itemTag = from x in doc.Descendants("item")
							select new NewsDetail()
							{
							Title = x.Element("title").Value,
							Description = x.Element("description").Value,
							Link = x.Element("link").Value,
							PubDate = DateTime.Parse(x.Element("pubDate").Value),
							ImageUrl = GetImageThumbnail(x).Item1,
							ImageWidth = GetImageThumbnail(x).Item2,
							ImageHeight = GetImageThumbnail(x).Item3
							};


						if (itemTag != null)
						{
							return itemTag.ToList();
						}
					}
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}
			return null;
		}


		private static Tuple<string, string, string> GetImageThumbnail(XElement x)
		{
			XNamespace mediaNameSpace = XNamespace.Get("http://search.yahoo.com/mrss/");

			if (x != null)
			{
				var elements = x.Elements(mediaNameSpace + "thumbnail");
				var url = elements.Select(t => t.Attribute("url").Value).SingleOrDefault();
				var width = elements.Select(t => t.Attribute("width").Value).SingleOrDefault();
				var height = elements.Select(t => t.Attribute("height").Value).SingleOrDefault();
				return Tuple.Create(url, width, height);
			}
			return null;
		}
	}
}
