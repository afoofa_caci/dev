﻿using System;
using System.Collections.Generic;

namespace NewsApp
{
	public interface IEmailService
	{
		void ShowDraft(string subject, string body, bool html, string to);

		bool CanSend { get; }
	}
}