﻿using System;
namespace NewsApp
{
	public class NewsDetail
	{
		public string Title
		{
			get;
			set;
		}

		public string Description
		{
			get;
			set;
		}

		public string Link
		{
			get;
			set;
		}

		public DateTime PubDate
		{
			get;
			set;
		}

		public string ImageUrl
		{
			get;
			set;
		}

		public string ImageWidth
		{
			get;
			set;
		}

		public string ImageHeight
		{
			get;
			set;
		}


	}
}
