﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsApp
{
	public class NewsViewModel
	{
		public List<NewsDetail> newsDetail; 

		public NewsViewModel()
		{
		}

		//Load the RSS, and parse it. Return the result order by Publishing date. 
		public async Task LoadNews(NewsSource newsSource)
		{
			var result = await NewsManager.Parser(newsSource);
			if (result != null)
			{
				newsDetail = result.OrderByDescending(x=>x.PubDate).ToList();
			}
		}
	}
}
