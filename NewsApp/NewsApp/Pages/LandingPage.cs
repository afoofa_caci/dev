﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace NewsApp
{
	public class LandingPage : ContentPage
	{
		StackLayout mainLayout;
		StackLayout listViewLayout;
		ListView lv;
		NewsViewModel vm;
		NewsSource CurrentActiveTab;

		public LandingPage()
		{
			Padding = new Thickness(0, 20, 0, 0);

			//Instead of using the Tab navigation. Using customTab will basically keep the UI same in between iOS and Android. 
			var customTab = new CustomTab();
			customTab.UKNewClicked += CustomTab_UKNewClicked;
			customTab.TechnolgyClicked += CustomTab_TechnolgyClicked;


			listViewLayout = new StackLayout()
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			mainLayout = new StackLayout()
			{
				Orientation = StackOrientation.Vertical,
				Children = { customTab , listViewLayout}	
			};

			Content = mainLayout;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			NavigationPage.SetHasNavigationBar(this, false);

			//Consume the RSS. 
			vm = new NewsViewModel();
			Task.Run(async () => {
				await vm.LoadNews(NewsSource.BBCUK).ContinueWith((arg) =>
				{
					LoadNewsLayout();
				});
			});
			CurrentActiveTab = NewsSource.BBCUK;
		}


		void CustomTab_UKNewClicked()
		{
			Debug.WriteLine("Clicked on UK news");
			Task.Run(async () =>
			{
				await vm.LoadNews(NewsSource.BBCUK).ContinueWith((arg) =>
				{
					LoadNewsLayout();
				});
			});
			CurrentActiveTab = NewsSource.BBCUK;
		}

		void CustomTab_TechnolgyClicked()
		{
			Debug.WriteLine("Clicked on Tech news news");
			Task.Run(async () =>
			{
				await vm.LoadNews(NewsSource.BBCTECHNOLOGY).ContinueWith((arg) =>
				{
					LoadNewsLayout();
				});
			});
			CurrentActiveTab = NewsSource.BBCTECHNOLOGY;
		}


		//With the assumptions that the RSS(s) feed are having the common properties.
		//Both UK news and Tech News are using the same layout. 
		private void LoadNewsLayout()
		{
			Device.BeginInvokeOnMainThread(() =>
			{
				listViewLayout.Children.Clear();

				lv = new ListView(ListViewCachingStrategy.RecycleElement);
				lv.ItemTemplate = new DataTemplate(typeof(NewsCell));
				lv.ItemsSource = vm.newsDetail;
				lv.HasUnevenRows = true;
				lv.IsPullToRefreshEnabled = true;

				listViewLayout.Children.Add(lv);

				lv.ItemSelected += async (sender, e) =>
				{
					var newsDetail = (NewsDetail)e.SelectedItem;
					await Navigation.PushAsync(new ArticlePage(newsDetail));
				};


				lv.Refreshing += async (sender, e) =>
				{
					await vm.LoadNews(CurrentActiveTab).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
						lv.IsRefreshing = false;
						LoadNewsLayout();
						});
					});
				};
			});


		}

	}
}

