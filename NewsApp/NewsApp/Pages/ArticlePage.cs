﻿using System;

using Xamarin.Forms;

namespace NewsApp
{
	public class ArticlePage : ContentPage
	{
		StackLayout mainLayout;

		public ArticlePage(NewsDetail newsDetail)
		{
			Title = "Article Page";

			//Web View
			WebView webview = new WebView
			{
				Source = newsDetail.Link,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			Button btnShare = new Button
			{
				Text = "Share",
				IsVisible = (Device.OS == TargetPlatform.iOS)
			};
			btnShare.Clicked += (sender, e) =>
			{
				var mailService = DependencyService.Get<IEmailService>();
				mailService.ShowDraft("Share" + newsDetail.Title, newsDetail.Description + " " + newsDetail.Link, true, "abc@gmail.com");
			};

			mainLayout = new StackLayout
			{
				Children = { webview, btnShare}
			};

			Content = mainLayout;
		}
	}
}

