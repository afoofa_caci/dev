﻿using System;
namespace NewsApp
{
	public static class Util
	{
		public static string AsRelativeDate(this DateTime date)
		{
			DateTime utcDate = date.ToUniversalTime();
			var ts = new TimeSpan(DateTime.UtcNow.Ticks - utcDate.Ticks);
			double delta = Math.Abs(ts.TotalSeconds);

			int SECOND = 1;
			int MINUTE = 60 * SECOND;
			int HOUR = 60 * MINUTE;
			int DAY = 24 * HOUR;
			int MONTH = 30 * DAY;

			if (delta < 0)
			{
				return "Not yet";
			}
			if (delta < 1 * MINUTE)
			{
				return "Just now";
			}
			if (delta < 2 * MINUTE)
			{
				return "A minute ago";
			}
			if (delta < 60 * MINUTE)
			{
				return ts.Minutes + " minutes ago";
			}
			if (delta < 120 * MINUTE)
			{
				return "An hour ago";
			}
			if (delta < 24 * HOUR)
			{
				return ts.Hours + " hours ago";
			}
			if (delta < 48 * HOUR)
			{
				return "Yesterday";
			}
			if (delta < 30 * DAY)
			{
				return ts.Days + " days ago";
			}
			if (delta < 12 * MONTH)
			{
				int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
				return months <= 1 ? "One month ago" : months + " months ago";
			}
			else
			{
				int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
				return years <= 1 ? "One year ago" : years + " years ago";
			}
		}
	}
}
