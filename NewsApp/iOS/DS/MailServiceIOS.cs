﻿using System;
using System.IO;
using MessageUI;
using NewsApp.iOS;
using UIKit;
using Xamarin.Forms;

[assembly: DependencyAttribute(typeof(MailServiceIOS))]
namespace NewsApp.iOS
{
	public class MailServiceIOS : IEmailService
	{
		private MFMailComposeViewController mailer;

		public MailServiceIOS()
		{
			if (MFMailComposeViewController.CanSendMail)
			{
				mailer = new MFMailComposeViewController();
				mailer.ModalPresentationStyle = UIModalPresentationStyle.CurrentContext;
			}
		}

		public bool CanSend
		{
			get
			{
				return MFMailComposeViewController.CanSendMail;
			}
		}


		public void ShowDraft(string subject, string body, bool html, string to)
		{
			if (!MFMailComposeViewController.CanSendMail)
			{
				return;
			}

			if (mailer == null)
			{
				mailer = new MFMailComposeViewController();
				mailer.ModalPresentationStyle = UIModalPresentationStyle.CurrentContext;
			}
			mailer.SetMessageBody(body ?? string.Empty, html);
			mailer.SetSubject(subject ?? string.Empty);
			mailer.SetToRecipients(new string[] { to });

			mailer.Finished += (s, e) => ((MFMailComposeViewController)s).DismissViewController(true, () =>
			{
				mailer = new MFMailComposeViewController();
				mailer.ModalPresentationStyle = UIModalPresentationStyle.CurrentContext;
			});



			Device.BeginInvokeOnMainThread(() =>
			{
				UIViewController vc = UIApplication.SharedApplication.KeyWindow.RootViewController;
				if (vc == null)
				{
					return;
				}
				while (vc.PresentedViewController != null)
				{
					vc = vc.PresentedViewController;
				}
				vc.PresentViewController(mailer, true, null);
			});
		}

		private string GetMimeType(string filePath)
		{
			if (string.IsNullOrEmpty(filePath))
			{
				return null;
			}

			var extension = Path.GetExtension(filePath.ToLowerInvariant());

			switch (extension)
			{
				case ".db3":
					return "application/x-sqlite3";
				//return "application/x-myiphone-app";
				case "png":
					return "image/png";
				case "doc":
					return "application/msword";
				case "pdf":
					return "application/pdf";
				case "jpeg":
				case "jpg":
					return "image/jpeg";
				case "zip":
				case "docx":
				case "xlsx":
				case "pptx":
					return "application/zip";
				case "htm":
				case "html":
					return "text/html";
			}

			return "application/octet-stream";
		}
	}
}
