This app is built using Xamarin Studio with Xamarin.Forms. 

There are 3 project inside the solutions: 

1. NewsApp (Xamarin.Forms)

2. NewsApp.Droid

3. NewsApp.iOS


It is ready for both iOS and Android. 







Min requirement: 

Min Android version: 4.0.3 (API 15)

Min iOS version: 8.0




There are only 2 Rss sources being used in the app. One is "UK News", and another one is "Technology News". 

Basic functionalities include:

* Load news articles from a public news feed (which is BBC UK News and BBC Technology News). 

* Display a scrollable list of news articles.

* Provide the option to filter news articles by category (such as UK News and Technology news), where user should see 2 tabs available on the top of the screen. Easy it is to toggle from 1 to another. 

* Show a single news article on screen by clicking on an article cell. 




Additional functionality, including:

* Provide the option to share news articles via email. This is available in the full article page. Unfortunately, I only managed to do it on iOS with the limited time. 

* Display a thumbnail of each article in the list of articles.

* Present news articles in the order, in which they are published. On top of that, instead of showing the publishing date. It shows the relative date such as 3 min before, 1 day before, etc. 

* Pull-to-refresh the article - pulling down the list will basically update the articles automatically. 





Things not included, but can be achieved:

1. Email can be shared in Android, but it does required more time to code around it. 

2. Option to share via Social Media - This is achievable with the integration of Xamarin.Auth. Once the integration is there, the app should be able to share the article to LinkedIn, Twitter, etc via their API. 

3. Download articles so they can be read offline - This is achievable by storing the RSS restring into a data storage such as to a file storage or into SQLite. Besides, it is also required to get the status of internet conection, where will help to tell when to show the content from cache. Or the other word, the app can show the content from the local storage whenever it is offline. 







Regards,
Jeff